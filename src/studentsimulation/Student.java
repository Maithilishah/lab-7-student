/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentsimulation;

/**
 *
 * @author Admin
 */
public class Student extends Person {
    
    private String studentid;

    public Student(String studentid, String fname, String lname) {
        super(fname, lname);
        this.studentid = studentid;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }
    
    
}
